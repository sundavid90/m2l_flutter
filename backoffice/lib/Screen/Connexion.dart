import 'package:flutter/material.dart';
import 'package:backoffice/Services/Product.dart';

class AuthService extends StatefulWidget {
  const AuthService({Key? key}) : super(key: key);

  @override
  State<AuthService> createState() => _AuthServiceState();
}

class _AuthServiceState extends State<AuthService> {
  late Future<List> _connecter;
  TextEditingController _pseudoController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
              'assets/logo.png', // Change this to the path of your logo image
              width: 150, // Adjust width as needed
              height: 150, // Adjust height as needed
            ),
            const SizedBox(height: 20),
              TextField(
                controller: _pseudoController,
                decoration: InputDecoration(
                  labelText: 'Pseudo',
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password',
                ),
              ),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () {
                  _login();
                },
                child: const Text('Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _login() async {
  String login = _pseudoController.text.trim();
  String mdp = _passwordController.text.trim();
  try {
    // Appel de la méthode de connexion avec les informations de l'utilisateur
    List<Map<String, dynamic>>? userData = await Connexion.connection(login, mdp);
  
    // Si la connexion réussit et que des données utilisateur sont retournées
    if (userData != null && userData.isNotEmpty) {
      // Affichez un message de succès ou effectuez d'autres actions en fonction de votre application
      print("Connexion réussie !");
      
      // Après une connexion réussie, vous pouvez naviguer vers la page des produits
      Navigator.pushReplacementNamed(context, '/productList');
    } else {
      // Si aucune donnée utilisateur n'est retournée, affichez un message d'erreur
      print("Identifiants incorrects !");
    }
  } catch (error) {
    // En cas d'erreur lors de la connexion, affichez un message d'erreur
    print("Erreur lors de la connexion : $error");
  }
}
}