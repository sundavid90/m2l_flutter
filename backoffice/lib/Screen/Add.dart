import 'package:flutter/material.dart';
import 'package:backoffice/Services/Product.dart';

class AddProductPage extends StatefulWidget {
  const AddProductPage({Key? key}) : super(key: key);

  @override
  _AddProductPageState createState() => _AddProductPageState();
}

class _AddProductPageState extends State<AddProductPage> {
  TextEditingController _nomController = TextEditingController();
  TextEditingController _stockController = TextEditingController();
  TextEditingController _sportController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _prixController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFc92b39),
        title: const Text('Ajouter un produit',style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: _nomController,
                decoration: InputDecoration(
                  labelText: 'Nom',
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _stockController,
                decoration: InputDecoration(
                  labelText: 'Stock',
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _sportController,
                decoration: InputDecoration(
                  labelText: 'Sport',
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _descriptionController,
                decoration: InputDecoration(
                  labelText: 'Description',
                ),
              ),
              const SizedBox(height: 16.0),
              TextField(
                controller: _prixController,
                decoration: InputDecoration(
                  labelText: 'Prix',
                ),
              ),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () {
                  _addProduct();
                },
                child: const Text('Ajouter'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _addProduct() async {
    String nom = _nomController.text.trim();
    int stock = int.tryParse(_stockController.text.trim()) ?? 0;
    String sport = _sportController.text.trim();
    String description = _descriptionController.text.trim();
    double prix = double.tryParse(_prixController.text.trim()) ?? 0.0;
    try {
      // Call the method to add product with the provided information
      int affectedRows = await Add.ajouterProduit(nom, stock, sport, description, prix);
  
      // If adding the product succeeds
      if (affectedRows > 0) {
        print("Product added successfully!");
        setState(() {
        // Actualiser les données après la mise à jour du produit
      });

      // Rediriger vers la page des produits après la mise à jour
      Navigator.pushReplacementNamed(context, '/productList');
      } else {
        print("Failed to add product!");
        // Handle the case where the product addition fails
      }
    } catch (error) {
      // If an error occurs during product addition
      print("Error adding product: $error");
    }
  }
}
