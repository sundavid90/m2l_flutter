import 'package:flutter/material.dart';

class ProduitDetailsMod extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails du Produit'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Ligne 1 : Image
            Image.asset(
              "chemin/vers/l'image.jpg",
              height: 150, // Ajustez la hauteur selon vos besoins
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 2 : Nom du produit et bouton de suppression
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Text(
                  'Nom du Produit',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () {
                    // Action à effectuer lors du clic sur l'icône de suppression
                  },
                ),
              ],
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 3 : Prix
            const Text(
              'Prix: 10€', // Utilisez votre variable de prix ici
              style: TextStyle(fontSize: 18),
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 4 : Formulaire pour modifier le prix
            Row(
              children: <Widget>[
                const Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Modifier le prix...",
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.check),
                  onPressed: () {
                    // Action à effectuer lors de la validation du prix
                  },
                ),
              ],
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 5 : Stock
            const Text(
              'Stock: 100', // Utilisez votre variable de stock ici
              style: TextStyle(fontSize: 18),
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 6 : Formulaire pour modifier le stock
            Row(
              children: <Widget>[
                const Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Modifier le stock...",
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.check),
                  onPressed: () {
                    // Action à effectuer lors de la validation du stock
                  },
                ),
              ],
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 7 : Bouton Description
            Center(
              child: ElevatedButton.icon(
                onPressed: () {
                  // Action à effectuer lors du clic sur le bouton de description
                },
                icon: const Icon(Icons.edit),
                label: const Text('Description'),
              ),
            ),
            const SizedBox(height: 10), // Espacement

            // Ligne 8 : Description
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 3),
              child: const Text(
                'Description du produit...',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
