import 'package:flutter/material.dart';
import 'package:backoffice/Services/Product.dart';
import 'package:flutter/cupertino.dart';

class AffichageProduit extends StatefulWidget {
  const AffichageProduit({Key? key}) : super(key: key);

  @override
  State<AffichageProduit> createState() => _AffichageProduitState();
}

class _AffichageProduitState extends State<AffichageProduit> {
  late Future<List> _product;

  @override
  void initState() {
    super.initState();
    _product = Produit.getAllProduct();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFc92b39),
        leading: Container(
          child: Image.asset(
            'assets/logo.png',
          ),
        ),
        title: const Text(
          "Produits",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
            },
          ),
        ],
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add');
        },
        backgroundColor: Colors.red,
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      body: FutureBuilder<List>(
        future: _product,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    ListTile(
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(snapshot.data![index]['nom'],
                              style: const TextStyle(fontSize: 35)),
                          Text(snapshot.data![index]['prix'].toString(),
                              style: const TextStyle(fontSize: 20)),
                          Text(snapshot.data![index]['sport'],
                              style: const TextStyle(fontSize: 20)),
                        ],
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            onPressed: () {
                              int id = snapshot.data![index]['id']; // Récupérer l'ID du produit
                              Navigator.pushNamed(context, '/update', arguments: id); // Naviguer vers la page de mise à jour avec l'ID du produit
                            },
                            icon: Icon(CupertinoIcons.pen),
                          ),
                          IconButton(
                            onPressed: () async {
                              int id = snapshot.data![index]['id'];
                              String nom = snapshot.data![index]['nom']; // Récupérer le nom du produit
                              // Afficher un message de confirmation avec des boutons Valider et Annuler
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Confirmation'),
                                    content: Text('Êtes-vous sûr de vouloir supprimer $nom ?'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop(); // Annuler la suppression
                                        },
                                        child: Text('Annuler'),
                                      ),
                                      TextButton(
                                        onPressed: () async {
                                          Navigator.of(context).pop(); // Valider la suppression
                                          try {
                                            int statusCode = await Delete.deleteProduct(id);
                                            if (statusCode == 200) {
                                              // Suppression réussie, rafraîchir la liste des produits si nécessaire
                                              setState(() {
                                                _product = Produit.getAllProduct();
                                              });
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                SnackBar(
                                                  content: Text('Le produit $nom a été supprimé avec succès.'),
                                                ),
                                              );
                                            } else {
                                              // Gérer d'autres codes d'état HTTP si nécessaire
                                              throw Exception('Erreur lors de la suppression du produit: $statusCode');
                                            }
                                          } catch (error) {
                                            print('Erreur lors de la suppression du produit: $error');
                                          }
                                        },
                                        child: Text('Valider'),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            icon: Icon(CupertinoIcons.delete),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1.0,
                      color: Colors.grey,
                    ),
                  ],
                );
              },
            );
          } else {
            return const Center(
              child: Text("Pas de données"),
            );
          }
        },
      ),
    );
  }
}
