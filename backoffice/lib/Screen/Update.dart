import 'package:flutter/material.dart';
import 'package:backoffice/Services/Product.dart'; 

class UpdateProductPage extends StatefulWidget {
  final int id;

  const UpdateProductPage({Key? key, required this.id}) : super(key: key);

  @override
  _UpdateProductPageState createState() => _UpdateProductPageState();
}

class _UpdateProductPageState extends State<UpdateProductPage> {
  TextEditingController _nomController = TextEditingController();
  TextEditingController _stockController = TextEditingController();
  TextEditingController _sportController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _prixController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _fetchProductDetails(); // Fetch product details when the page initializes
  }

  void _fetchProductDetails() async {
    try {
      // Fetch product details based on the provided ID using the Product service class
      List<dynamic> productData = await Produit.getAllProduct();
      // Find the product with the provided ID
      Map<String, dynamic>? product = productData.firstWhere((element) => element['id'] == widget.id, orElse: () => null);

      if (product != null) {
        // Set the text controllers with the fetched product details
        _nomController.text = product['nom'];
        _stockController.text = product['stock'].toString();
        _sportController.text = product['sport'];
        _descriptionController.text = product['description'];
        _prixController.text = product['prix'].toString();
      } else {
        // Handle case where product with provided ID is not found
        print('Product with ID ${widget.id} not found');
      }
    } catch (error) {
      print('Error fetching product details: $error');
      // Handle error if product details fetching fails
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFc92b39),
        title: const Text('Modifier le produit',style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),),
        
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _nomController,
              decoration: InputDecoration(
                labelText: 'Nom',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _stockController,
              decoration: InputDecoration(
                labelText: 'Stock',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _sportController,
              decoration: InputDecoration(
                labelText: 'Sport',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _descriptionController,
              decoration: InputDecoration(
                labelText: 'Description',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _prixController,
              decoration: InputDecoration(
                labelText: 'Prix',
              ),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                _updateProduct();
              },
              child: const Text('Modifier'),
            ),
          ],
        ),
      ),
    );
  }

  void _updateProduct() async {
    String nom = _nomController.text.trim();
    int stock = int.tryParse(_stockController.text.trim()) ?? 0;
    String sport = _sportController.text.trim();
    String description = _descriptionController.text.trim();
    double prix = double.tryParse(_prixController.text.trim()) ?? 0.0;
    try {
      // Call the method to update the product with the provided information using the Update service class
      int statusCode = await Update.updateProduct(widget.id, nom, stock, sport, description, prix);
  
      // If the product update is successful
      if (statusCode == 200) {
        print("Produit mis à jour avec succès !");
        // Refresh data after product update
        _fetchProductDetails(); // Optionally, you can refresh the data by refetching product details
        // Redirect to the product list page after the update
        Navigator.pushReplacementNamed(context, '/productList');
      } else {
        print("Échec de la mise à jour du produit !");
        // Handle case where product update fails
      }
    } catch (error) {
      // If an error occurs during product update
      print("Erreur lors de la mise à jour du produit : $error");
    }
  }
}
