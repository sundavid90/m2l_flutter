import 'dart:convert';
import 'package:http/http.dart' as http;

class Produit {
  static String baseUrl = "http://10.0.2.2:3000";

  static Future<List> getAllProduct() async {
    try {
      var res = await http.get(Uri.parse("$baseUrl/produit/produit"));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }
}

class Connexion {
  static String baseUrl = "http://10.0.2.2:3000";

  static Future<List<Map<String, dynamic>>> connection(
      String login, String mdp) async {
    try {
      var url = Uri.parse("$baseUrl/user/connexion");
      var response = await http.post(
        url,
        body: jsonEncode({'login': login, 'mdp': mdp}),
        headers: {'Content-Type': 'application/json'},
      );

      print('Response Status Code: ${response.statusCode}');

      if (response.statusCode == 200) {
        // Convertir la réponse en un seul objet
        var userData = jsonDecode(response.body);
        // Vérifier si l'utilisateur est administrateur
        if (userData['role'] == 'admin') {
          // Encapsuler cet objet dans une liste
          List<Map<String, dynamic>> userDataList = [userData];
          return userDataList;
        } else {
          // Utilisateur n'est pas administrateur, renvoyer une erreur
          throw Exception("Vous n'êtes pas administrateur. Accès refusé.");
        }
      } else {
        // Gérer les autres codes d'état ici
        throw Exception('Server error: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }
}

class Add {
  static String baseUrl = "http://10.0.2.2:3000"; // Assuming your server is running on this address

  static Future<int> ajouterProduit(String nom, int stock, String sport, String description, double prix) async {
    try {
      var url = Uri.parse("$baseUrl/produit/ajouter");
      var response = await http.post(
        url,
        body: jsonEncode({
          'nom': nom,
          'stock': stock,
          'sport': sport,
          'description': description,
          'prix': prix,
        }),
        headers: {'Content-Type': 'application/json'},
      );

      print('Response Status Code: ${response.statusCode}');

      if (response.statusCode == 200) {
        // If the request is successful, return the affected rows
        return jsonDecode(response.body) as int;
      } else {
        // Handle other status codes here
        throw Exception('Server error: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }
}

class Delete {
  static String baseUrl = "http://10.0.2.2:3000";

  static Future<int> deleteProduct(int id) async {
    try {
      var url = Uri.parse("$baseUrl/produit/supprimer/$id");
      var response = await http.put(url);

      if (response.statusCode == 200) {
        // La suppression s'est effectuée avec succès
        return response.statusCode;
      } else {
        // Gérer d'autres codes d'état HTTP si nécessaire
        throw Exception('Erreur lors de la suppression du produit: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Erreur lors de la suppression du produit: $error');
    }
  }
}

class Update {
  static String baseUrl = "http://10.0.2.2:3000"; // Assuming your server is running on this address

  static Future<int> updateProduct(int id, String nom, int stock, String sport, String description, double prix) async {
    try {
      var url = Uri.parse("$baseUrl/produit/modifier/$id");
      var response = await http.put(
        url,
        body: jsonEncode({
          'nom': nom,
          'stock': stock,
          'sport': sport,
          'description': description,
          'prix': prix,
        }),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 200) {
        // If the request is successful, return the status code
        return response.statusCode;
      } else {
        // Handle other status codes here
        throw Exception('Error updating product: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Error updating product: $error');
    }
  }
}