import 'package:flutter/material.dart';
import 'package:backoffice/Screen/Product.dart';
import 'package:backoffice/Screen/Connexion.dart';
import 'package:backoffice/Screen/Add.dart';
import 'package:backoffice/Screen/Update.dart';

void main() {
  runApp(const MaterialApp(
    home: Home(),
  ));
}

class Home extends StatelessWidget {
  const Home({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Backoffice',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/', // Set the initial route to '/'
      routes: {
        '/': (context) => AuthService(), // Route for the login page
        '/productList': (context) => AffichageProduit(), // Route for the product page
        '/add': (context) => AddProductPage(),
        '/update': (context) {
        final id = ModalRoute.of(context)!.settings.arguments as int;
        return UpdateProductPage(id: id);
        },
      },
    );
  }
}
