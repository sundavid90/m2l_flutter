-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le : mer. 27 mars 2024 à 13:50
-- Version du serveur : 10.6.5-MariaDB
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `m2l_v2`
--

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `sport` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `prix` decimal(10,2) NOT NULL,
  `isSee` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `stock`, `sport`, `description`, `image`, `prix`, `isSee`) VALUES
(1, 'Sabre Luke Skywalker', 50, 'Sabre Laser Sportif', 'Inspirée de la saga, cette arme est l\'une des plus appréciées de notre armurerie en ligne. Son manche ergonomique spiralé assure une prise en main confortable, facilitant la manipulation lors des combats en duel et faisant de toi un Jedi redoutable. Cette arme polyvalente est également une pièce de collection idéale à exposer sur un support.', '/images/sabre_luke.jpg', '30.00', b'1'),
(2, 'Sabre Star Killer', 50, 'Sabre Laser Sportif', 'Le Star Killer présente un design élégant avec une chambre à cristaux assortie à la couleur de la lame. C\'est le sabre laser parfait pour les passionnés et les collectionneurs. Non seulement vous disposez d\'une poignée en aluminium aéronautique T6 durable à saisir, mais également d\'une vaste gamme de paysages sonores et d\'effets de lumière qui rehausseront la présentation globale de ce sabre unique.', '/images/sabre_star.jpg', '30.00', b'1'),
(3, 'Sabre Anakin Skywalker', 50, 'Sabre Laser Sportif', 'Avec une poignée de 28 cm en alliage d\'aluminium haut de gamme, ce sabre laser t\'offre une sensation authentique et une résistance à toute épreuve. Les détails sont soignés, des rainures noires verticales aux boutons de contrôle, en passant par les éléments finement reproduits qui donnent à ce sabre laser un réalisme saisissant.', '/images/sabre_anakin.jpg', '30.00', b'1'),
(4, 'Gants de Gardien de But', 44, 'Football', 'Te protège et te donne bonne mine : Découvre le WHITE BEAST 3.0 avec une protection efficace des doigts. Supe Gecko Grip de 4mm. Négative CUT. Protections de doigts amovibles. Respirant.', '/images/gants.jpg', '30.00', b'1'),
(5, 'Protège Tibias', 48, 'Football', 'Créés pour t\'offrir une vitesse ultime, ces protège-tibias de football Pro font partie de la famille adidas X dédiée au mouvement. Leur coque légère et souple s\'adapte parfaitement pour t\'offrir la meilleure défense possible en bas des jambes. Le rembourrage en EVA assure un confort supérieur tout en amortissant les chocs. Les manchons de compression les maintiennent bien en place.Fabriqués avec au moins 20 % de matériaux recyclés pour réduire l\'impact environnemental important de la production de matériaux vierges.', '/images/protege.jpg', '30.00', b'1'),
(6, 'Ballon de Football', 50, 'Football', 'Fabriqué à partir d’un nouveau cuir synthétique PU avec une surface structurée et d\'une doublure en néoprène de 2 mm pour plus de souplesse et d’une vessie butyle pour une parfaite rétention de l\'air. Les 32 panneaux cousus main assurent une trajectoire rectiligne dans les airs.', '/images/ballon.jpg', '30.00', b'1'),
(7, 'Arc', 49, 'Tir à l\'arc', 'Vous cherchez à progresser davantage en tir à l\'arc en CLUB ? Nous avons développé ce set, un arc classique, poignée en aluminium forgé pour vous accompagner dans vos premiers pas en compétition', '/images/arc.jpg', '30.00', b'1'),
(8, 'Carquois', 50, 'Tir à l\'arc', 'Vous cherchez une solution simple et rapide pour transporter vos bâtons de trail ? Rangez et attrapez facilement vos bâtons pour gagner du temps pendant vos courses.', '/images/carquois.jpg', '30.00', b'1'),
(9, 'Flèche x3', 50, 'Tir à l\'arc', 'Vous cherchez une flèche précise et au look efficace pour tirer en club avec un arc classique ? Nous avons développé cette flèche pour vous accompagner sur les pas de vos premières compétitions.', '/images/fleches.jpg', '30.00', b'1'),
(10, 'Chasuble', 25, 'Football', 'Indispensable pour le football, une chasuble de foot est un article qui permet de différencier les joueurs lors de vos séances d’entrainements, mais aussi pour identifier vos remplaçants lors de vos rencontres de foot et obligatoire pour les échauffements. Click For Foot, met à disposition des clubs amateurs et professionnels, des chasubles de football pour les catégories des enfants et des adultes avec différent modèle : simple, qualité, extensible, numérotée, réversible et velcro. Retrouvez également des articles plus ludiques pour vos entraînements comme des chasubles bicolores, des écharpes de jeu ajustables et des foulards de jeu, une alternative à la chasuble pour les plus jeunes. Conçu pour constituer et différencier rapidement deux équipes.', '/images/image_1702392701420-605634530.jpg', '56.00', b'0'),
(11, 'Chevalet pour cible', 50, 'Tir à l\'arc', 'Chevalet en bois autoclave. Idéal pour les cibles rondes et carrées. Hauteur : 170 cm. Largeur : 80 cm.', '/images/image_1702303103375-966582678.jpg', '10.00', b'0');

-- --------------------------------------------------------

--
-- Structure de la table `produit_ap4`
--

DROP TABLE IF EXISTS `produit_ap4`;
CREATE TABLE IF NOT EXISTS `produit_ap4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `sport` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prix` decimal(10,2) NOT NULL,
  `isSee` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit_ap4`
--

INSERT INTO `produit_ap4` (`id`, `nom`, `stock`, `sport`, `description`, `prix`, `isSee`) VALUES
(1, 'Sabre Luke Skywalker', 50, 'Sabre Laser Sportif', 'Inspirée de la saga, cette arme est l\'une des plus appréciées de notre armurerie en ligne. Son manche ergonomique spiralé assure une prise en main confortable, facilitant la manipulation lors des combats en duel et faisant de toi un Jedi redoutable. Cette arme polyvalente est également une pièce de collection idéale à exposer sur un support.', '30.00', b'1'),
(2, 'Sabre Star Killer', 50, 'Sabre Laser Sportif', 'Le Star Killer présente un design élégant avec une chambre à cristaux assortie à la couleur de la lame. C\'est le sabre laser parfait pour les passionnés et les collectionneurs. Non seulement vous disposez d\'une poignée en aluminium aéronautique T6 durable à saisir, mais également d\'une vaste gamme de paysages sonores et d\'effets de lumière qui rehausseront la présentation globale de ce sabre unique.', '30.00', b'1'),
(3, 'Sabre Anakin Skywalker', 50, 'Sabre Laser Sportif', 'Avec une poignée de 28 cm en alliage d\'aluminium haut de gamme, ce sabre laser t\'offre une sensation authentique et une résistance à toute épreuve. Les détails sont soignés, des rainures noires verticales aux boutons de contrôle, en passant par les éléments finement reproduits qui donnent à ce sabre laser un réalisme saisissant.', '30.00', b'1'),
(4, 'Gants de Gardien de But', 44, 'Football', 'Te protège et te donne bonne mine : Découvre le WHITE BEAST 3.0 avec une protection efficace des doigts. Supe Gecko Grip de 4mm. Négative CUT. Protections de doigts amovibles. Respirant.', '30.00', b'1'),
(5, 'Protège Tibias', 48, 'Football', 'Créés pour t\'offrir une vitesse ultime, ces protège-tibias de football Pro font partie de la famille adidas X dédiée au mouvement. Leur coque légère et souple s\'adapte parfaitement pour t\'offrir la meilleure défense possible en bas des jambes. Le rembourrage en EVA assure un confort supérieur tout en amortissant les chocs. Les manchons de compression les maintiennent bien en place.Fabriqués avec au moins 20 % de matériaux recyclés pour réduire l\'impact environnemental important de la production de matériaux vierges.', '30.00', b'1'),
(6, 'Ballon de Football', 50, 'Football', 'Fabriqué à partir d’un nouveau cuir synthétique PU avec une surface structurée et d\'une doublure en néoprène de 2 mm pour plus de souplesse et d’une vessie butyle pour une parfaite rétention de l\'air. Les 32 panneaux cousus main assurent une trajectoire rectiligne dans les airs.', '30.00', b'1'),
(7, 'Arc', 49, 'Tir à l\'arc', 'Vous cherchez à progresser davantage en tir à l\'arc en CLUB ? Nous avons développé ce set, un arc classique, poignée en aluminium forgé pour vous accompagner dans vos premiers pas en compétition', '30.00', b'1'),
(8, 'Carquois', 5, 'Tir  à l\'arc', 'test', '20.00', b'1'),
(9, 'Flèche x3', 50, 'Tir à l\'arc', 'Vous cherchez une flèche précise et au look efficace pour tirer en club avec un arc classique ? Nous avons développé cette flèche pour vous accompagner sur les pas de vos premières compétitions.', '30.00', b'1'),
(10, 'Chasuble', 2, 'Football', 'test', '5.00', b'1'),
(11, 'Chevalet pour cible', 50, 'Tir à l\'arc', 'Chevalet en bois autoclave. Idéal pour les cibles rondes et carrées. Hauteur : 170 cm. Largeur : 80 cm.', '10.00', b'0'),
(12, 'coupelle', 25, 'Football', 'Test', '15.00', b'0'),
(13, 'echelle', 25, 'Football', 'Test', '25.00', b'1');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produit` int(11) NOT NULL,
  `id_utilisateur` char(36) NOT NULL,
  `statut` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `quantite` int(255) NOT NULL,
  `note` text DEFAULT NULL,
  `commentaire` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_produit_2` (`id_produit`),
  KEY `id_produit` (`id_produit`),
  KEY `id_utilisateur` (`id_utilisateur`),
  KEY `id_utilisateur_2` (`id_utilisateur`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`id`, `id_produit`, `id_utilisateur`, `statut`, `date`, `quantite`, `note`, `commentaire`) VALUES
(50, 7, '827780d4-deeb-42db-ab22-3d5d18b42617', 'validé', '2023-12-07', 1, NULL, NULL),
(51, 8, '827780d4-deeb-42db-ab22-3d5d18b42617', 'en cours', '2023-12-07', 1, NULL, NULL),
(52, 9, '827780d4-deeb-42db-ab22-3d5d18b42617', 'en cours', '2023-12-07', 6, NULL, NULL),
(53, 4, '665b72f8-84fa-419a-a577-31252d306bd0', 'validé', '2023-12-07', 5, '2', 'dsqds'),
(54, 5, '665b72f8-84fa-419a-a577-31252d306bd0', 'validé', '2023-12-07', 4, '1', 'fdsfsd'),
(55, 6, '665b72f8-84fa-419a-a577-31252d306bd0', 'refusé', '2023-12-07', 3, NULL, NULL),
(59, 1, '665b72f8-84fa-419a-a577-31252d306bd0', 'en cours', '2024-02-14', 1, NULL, '<h1>text </h1>'),
(60, 2, '665b72f8-84fa-419a-a577-31252d306bd0', 'en cours', '2024-02-14', 1, '2', 'test'),
(61, 3, '665b72f8-84fa-419a-a577-31252d306bd0', 'en cours', '2024-02-14', 1, '3', 'defsdfqdsq');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` char(36) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `pseudo`, `email`, `role`, `password`) VALUES
('14bb8d9a-0c41-409a-bb7b-2e05d07b963f', 'dsun', 'sundavid@gmail.com', 'user', '$2b$10$M0RklgpI5DiWKVv0FDBWQ.EgsaGqlr8DQ/JhqyDwOdamXypLQo2dW'),
('4ef70683-e712-49bd-ab2c-9312858407e5', 'admin', 'admin@gmail.com', 'admin', '$2b$10$mtbO5UOhUH/U4ncNXALfkOnJw8sVSQruGLo/zBwwu7zqeUWs8dfmC'),
('665b72f8-84fa-419a-a577-31252d306bd0', 'david', 'david@gmail.com', 'user', '$2b$10$9c6HNGpxqBvkj9UlS2Kjz./tofB9lTkYQtAetVkY5VPGwpPoHSl.y'),
('827780d4-deeb-42db-ab22-3d5d18b42617', 'msun', 'msad@gmail.com', 'user', '$2b$10$UiujYabRFmDXUXK//MmJueIcHWgeFuiUbtkWkamTbnAFwSQlbQJai');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
